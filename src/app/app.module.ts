import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {DomainsComponent} from './domains/domains.component';
import {DomainComponent} from './domains/domain/domain.component';
import {DomainsListComponent} from './domains/domains-list/domains-list.component';
import {UserInputComponent} from './domains/user-input/user-input.component';
import {HttpModule} from "@angular/http";
import {DomainsService} from './domains/domains.service';


@NgModule({
  declarations: [
    AppComponent,
    DomainsComponent,
    DomainComponent,
    DomainsListComponent,
    UserInputComponent
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [DomainsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
