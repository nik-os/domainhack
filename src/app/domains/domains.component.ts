import {Component} from '@angular/core';
import {Domain} from './domain.model';


@Component({
  selector: 'app-domains',
  templateUrl: './domains.template.html'
})
export class DomainsComponent {
  domains: Domain[] = [];
}
