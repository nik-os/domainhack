import {Component} from '@angular/core';
import {DomainsService} from '../domains.service';
import {Domain} from '../domain.model';

@Component({
  selector: 'app-user-input',
  templateUrl: 'user-input.template.html'
})
export class UserInputComponent {

  constructor(private domainsService: DomainsService) {}

  onUserInputChange(event: Event) {
    const value = (<HTMLInputElement>event.target).value;

    if (value.length > 2) {
      this.domainsService.sendDomain((<HTMLInputElement>event.target).value)
      .subscribe();
    } else {
      this.domainsService.domainsListEdit.emit([]);
    }
  }

}
