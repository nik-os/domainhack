import {Component, OnInit} from '@angular/core';
import {Domain} from '../domain.model';
import {DomainsService} from '../domains.service';

@Component({
  selector: 'app-domains-list',
  templateUrl: 'domains-list.template.html'
})
export class DomainsListComponent implements OnInit {
  domains: Domain[] = [];

  constructor(private domainsService: DomainsService) {}

  ngOnInit() {
    this.domainsService.domainsListEdit.subscribe(
      (domains: Domain[]) => this.domains = domains
    );
  }
}
