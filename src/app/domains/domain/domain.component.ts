import {Component, Input, ViewEncapsulation} from '@angular/core';
import {Domain} from '../domain.model';

@Component({
  selector: '[app-domain]',
  templateUrl: 'domain.template.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
        .domain {
            font-weight: bold;
            color: gray;
        }
    `],
})
export class DomainComponent {
  @Input() domain: Domain;
}
