import {EventEmitter, Injectable} from '@angular/core';
import {Headers, Http, Response} from '@angular/http';

import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Domain} from './domain.model';
import {environment} from '../../environments/environment';

@Injectable()
export class DomainsService {
  private domains: Domain[] = [];

  private domainsCache = [];

  domainsListEdit = new EventEmitter<Domain[]>();

  constructor(private http: Http) {}

  sendDomain(value: string) {
    if (!this.domainsCache[value]) {
      const body = JSON.stringify({
        input: value
      });
      const headers = new Headers({'Content-Type': 'application/json'});
      return this.http.post(environment.apiUrl + '/domain', body, {headers: headers})
        .map((response: Response) => {

          this.domains = [];

          if (response.status === 200) {
            const domains = response.json().obj;

            for (const domain of domains) {
              this.domains.push(new Domain(domain.str, domain.desc));
            }
          }

          this.domainsCache[value] = this.domains;

          this.domainsListEdit.emit(this.domains);

          return this.domains;
        })
        .catch((error: Response) => {
          return Observable.throw(error);
        });
    } else {
      this.domains = this.domainsCache[value];
      this.domainsListEdit.emit(this.domains);
      return Observable.of(this.domainsCache[value]);
    }
  }
}

