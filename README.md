# Domain Hack

Compiled application is located in `dist` folder

### Installation

Install the dependencies:

```sh
$ npm install
```

Launch development server:

```sh
$ ng serve
```

Creating build 

```sh
$ ng build
```

### Configuration

Set api url in `src/environments/environments`
